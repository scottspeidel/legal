require "spec_helper"
require "people_parser"

describe PeopleParser do
  
  before(:each) do
     @pp = PeopleParser.new
  end
  
  it "Should parse file format 2 CSV into an array [last_name, first_name, gender, date_of_birth, favorite_color]" do 
    @pp.parse_csv_string_to_array("Abercrombie, Neil, Male, Tan, 2/13/1943").should == ["Abercrombie", "Neil", "Male", "2/13/1943","Tan"]
  end
         
  it "Should parse file format 1 Pipe Delimited into an array [last_name, first_name, gender, date_of_birth, favorite_color]" do
    @pp.parse_pdv_string_to_array("Smith | Steve | D | M | Red | 3-3-1985").should == ["Smith", "Steve", "Male", "3/3/1985","Red"]
  end 
  
  it "Should parse file format 3 Space Delimited into an array [last_name, first_name, gender, date_of_birth, favorite_color]" do
    @pp.parse_sdv_string_to_array("Kournikova Anna F F 6-3-1975 Red").should == ["Kournikova", "Anna", "Female", "6/3/1975","Red"]
  end              
  
  it "Should parse file format 3 Space Delimited with multiple spaces" do
    @pp.parse_sdv_string_to_array("Kournikova      Anna F F  6-3-1975     Red").should == ["Kournikova", "Anna", "Female", "6/3/1975","Red"]
  end
  
  it "Should raise an error if the GENDER is not recognized" do
    lambda {@pp.parse_sdv_string_to_array("Kournikova Anna X X 6-3-1975 Red")  }.should raise_error("ERROR: GENDER not recognized")
  end
  
  
end