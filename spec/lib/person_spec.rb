require "spec_helper"
require "person"

describe Person do
  
  it "Should create a person object" do
    person = Person.new("Hendrix", "Jimi", "Male", "11/27/1942","purple")
    person.first_name.should == "Jimi"
    person.last_name.should == "Hendrix"
    person.gender.should == "Male"
    person.date_of_birth.should == "11/27/1942" 
    person.favorite_color.should == "purple"
  end
  
  
end