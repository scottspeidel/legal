require 'util'

class PeopleParser
  
  LAST_NAME      =  0
  FIRST_NAME     =  1
  GENDER         =  2
  DOB            =  3
  COLOR          =  4 
  
  
  def parse_csv_string_to_array(csv_string)
    @person_array = csv_string.delete(' ').split(',')
    @person_array.swap!(DOB,COLOR)   
  end
  
  def parse_pdv_string_to_array(csv_string)
    @person_array = csv_string.delete(' ').split('|')
    @person_array.delete_at(2)                #remove unused data in field #2 shrink array from 6 to 5
    @person_array.swap!(DOB,COLOR)                  
    detect_and_convert_alt_gender_format()
    @person_array[DOB].gsub!('-','/')
    @person_array
  end
  
  def parse_sdv_string_to_array(csv_string)
    @person_array = csv_string.split(' ')
    @person_array.delete_at(2)                 #remove unused data in field #2 shrink array from 6 to 5
    detect_and_convert_alt_gender_format()
    @person_array[DOB].gsub!('-','/')
    @person_array
  end
  
  
  def detect_and_convert_alt_gender_format()
    gender = @person_array[GENDER] 
    if ["Male","M"].include? gender 
      gender = "Male"
    elsif ["Female","F"].include? gender 
      gender = "Female"
    else
      raise "ERROR: GENDER not recognized"
    end
    @person_array[GENDER] = gender
    @person_array
  end
  
  
  
    
  
  
  
end